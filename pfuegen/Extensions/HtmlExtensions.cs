﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using pfuegen.Disposables;
using pfuegen.Treasure;

namespace pfuegen.Extensions {
    public static class HtmlExtensions {

        public static MvcHtmlString PfDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
                                                                         Expression<Func<TModel, TProperty>> expression,
                                                                         IEnumerable<SelectListItem> selectList) {
            return MvcHtmlString.Create(
                "<div>" +
                htmlHelper.LabelFor(expression, new {@class="pf-drop-down-label"}).ToString() + 
                htmlHelper.DropDownListFor(expression, selectList, new {@class="pf-drop-down-list"})
                + "</div>");
        }

        public static MvcHtmlString PfLabel(this HtmlHelper html, string label){
            return html.Label(label, new { @class = "pf-info-label" }); 
        }

        public static MvcHtmlString PfText(this HtmlHelper html, string text) {
            return MvcHtmlString.Create("<div class=\"pf-info-text\">" + text + "</div>");
        }

        public static MvcHtmlString PfLabeledText(this HtmlHelper html, string label, string text) {
            return MvcHtmlString.Create(PfLabel(html, label) + PfText(html, text).ToString());
        }

        public static MvcHtmlString PfLabelFor<TModel, TResult>(this HtmlHelper<TModel> html, Expression<Func<TModel, TResult>> expression) {
            return html.LabelFor(expression, new { @class = "pf-info-label" });
        }

        public static MvcHtmlString PfLabelFor<TModel, TResult>(this HtmlHelper<TModel> html, string label, Expression<Func<TModel, TResult>> expression) {
            return html.LabelFor(expression, label, new { @class = "pf-info-label" });
        }

        public static MvcHtmlString PfDisplayTextFor<TModel, TResult>(this HtmlHelper<TModel> html, Expression<Func<TModel, TResult>> expression) {
            return MvcHtmlString.Create(html.PfLabelFor(expression) + "<div class=\"pf-info-text\">" + html.DisplayFor(expression) + "</div>");
        }

        public static MvcHtmlString PfDisplayTextFor<TModel, TResult>(this HtmlHelper<TModel> html, string label, Expression<Func<TModel, TResult>> expression) {
            return MvcHtmlString.Create(
                html.LabelFor(expression, label, new { @class = "pf-info-label" }) +
                "<div class=\"pf-info-text\">" + html.DisplayFor(expression) + "</div>");
        }

        public static MvcHtmlString PfActionLink<TModel>(this HtmlHelper<TModel> html, string text, string actionName) {
            return MvcHtmlString.Create("<span class=\"pf-link\">" + html.ActionLink(text, actionName) + "</span>");
        }

        public static PfForm PfForm(this HtmlHelper html, string label, string actionName, string controllerName) {
            return new PfForm(html, label, actionName, controllerName);
        }

        public static PfSection PfSection(this HtmlHelper html) {
            return new PfSection(html);
        }
    }
}