﻿using System;
using System.IO;
using System.Web.Mvc;

namespace pfuegen.Disposables {
    public class PfSection : IDisposable {

        private readonly TextWriter _writer;
        
        public PfSection(HtmlHelper html) {
            _writer = html.ViewContext.Writer;
            _writer.Write("<div class=\"pf-info\">");
        }

        public void Dispose() {
            _writer.Write("</div>");
        }
    }
}