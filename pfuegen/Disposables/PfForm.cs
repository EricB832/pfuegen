﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace pfuegen.Disposables {
    public class PfForm : IDisposable {
        private readonly TextWriter _writer;
        private readonly HtmlHelper _html;
        private string _label;
        
        public PfForm(HtmlHelper html, string label, string actionName, string controllerName) {
            _html = html;
            _writer = _html.ViewContext.Writer;
            _label = label;
            _html.BeginForm(actionName, controllerName, new { _html.ViewBag.ReturnUrl }, FormMethod.Post, new { @class="pf-info" });
        }

        public void Dispose() {
            _writer.Write("<div class=\"pf-submit\"><input type=\"submit\" value=\"" + _label + "\" name=\"gen\"/></div>");
            _html.EndForm();
        }
    }
}