﻿using pfuegen.Helpers;
using pfuegen.Treasure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfuegen.Generators {
    public static class WondrousGenerator {
        public const string Table = "5-1";
        public static Wondrous Generate(int grade) {
            var types = Data.Get(Table);
            var type = types.RolledRow<int>(Dice.Roll(100)).ToList();
            var items = Data.Get("5-" + type[1]);
            var item = items.RolledRowByGrade<string>(Dice.Roll(100), 3, grade).ToList();
            var cursed = Dice.Roll(100) == 1; 
            var intelligent = Dice.Roll(100) == 100;
            return new Wondrous {
                Cursed = cursed,
                Intelligent = intelligent,
                Name = item[1],
                Value = int.Parse(item[2])
            };
        }
    }
}