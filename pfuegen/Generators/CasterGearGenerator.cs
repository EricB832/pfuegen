﻿using pfuegen.Helpers;
using pfuegen.Treasure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace pfuegen.Generators {
    public static class CasterGearGenerator {
        public const string Table = "7-9";
        public static Tuple<Coins, IEnumerable<Small>, IEnumerable<Armament>, IEnumerable<Wondrous>> Generate(int value) {
            Debug.WriteLine("Magic: " + value);
            var data = Data.Get(Table);
            var values = data.RandomRowByValue<string>(value).ToList();
            var coins = GetCoins(values);
            var small = GetSmall(values);
            var arms = GetArms(values);
            var wondrous = GetWondrous(values);
            return Tuple.Create(coins, small, arms, wondrous);
        }

        private static IEnumerable<Wondrous> GetWondrous(List<String> values) {
            var wondrous = Generator.Parse(values[7]);
            var rings = Generator.Parse(values[8]);
            var rods = Generator.Parse(values[9]);
            var staves = Generator.Parse(values[10]);
            return Generator.Generate(wondrous, WondrousGenerator.Generate).Concat(
                Generator.Generate(rings, RingGenerator.Generate)).Concat(
                Generator.Generate(rods, RodGenerator.Generate)).Concat(
                Generator.Generate(staves, StaffGenerator.Generate));
        }

        private static IEnumerable<Armament> GetArms(List<String> values) {
            var arms = new List<Armament>();
            if (values[6] != "0") {
                int grade;
                if (int.TryParse(values[6], out grade)) {
                    arms.Add(WeaponGenerator.Generate(grade));
                } else {
                    arms.Add(WeaponGenerator.Generate(values[6]));
                }
            }
            return arms;
        }

        private static IEnumerable<Small> GetSmall(List<string> values) {
            var small = new List<Small>();
            var potions = Generator.Parse(values[3]);
            small.AddRange(Generator.Generate(potions, PotionGenerator.Generate));
            var scrolls = Generator.Parse(values[4]);
            small.AddRange(Generator.Generate(scrolls, ScrollGenerator.Generate));
            var wands = Generator.Parse(values[5]);
            small.AddRange(Generator.Generate(wands, WandGenerator.Generate));
            return small;
        }

        private static Coins GetCoins(List<string> values) {
            return new Coins {
                Silver = Dice.Roll(values[1]),
                Gold = Dice.Roll(values[2])
            };
        }
    }
}