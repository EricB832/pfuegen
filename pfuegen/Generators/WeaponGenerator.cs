﻿using pfuegen.Helpers;
using pfuegen.Treasure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfuegen.Generators {
    public class WeaponGenerator {
        public const string TypeTable = "7-13";
        public const string BonusTable = "3-7";
        public const string MeleeTable = "3-8";
        public const string RangedTable = "3-10";
        public const string SpecificTable = "3-12";
        public static Armament Generate(int grade) {
            Armament armament = new Armament();
            var bonuses = Data.Get(BonusTable + "-" + grade);
            var bonus = bonuses.RolledRow<string>(Dice.Roll(100)).ToList();
            var abonus = ParseBonus(bonus[2]);
            if (abonus.Item1 == 0 && bonus[1] == "0") {
                var specifics = Data.Get(SpecificTable + "-" + grade);
                var specific = specifics.RolledRow<string>(Dice.Roll(100)).ToList();
                armament.Name = specific[1];
                armament.Value = int.Parse(specific[2]);
                return armament;
            }

            var types = Data.Get(TypeTable);
            var type = types.RolledRow<string>(Dice.Roll(100)).ToList();
            armament.Name = type[1];
            armament.BaseValue = int.Parse(type[2]);
            armament.Bonus = int.Parse(bonus[1]);
            if (abonus.Item1 != 0) {
                var table = type[3] == "M" ? MeleeTable : RangedTable;
                var abilities = Data.Get(table + "-" + abonus.Item2);
                for (int i = 0; i < abonus.Item1; i++) {
                    var ability = abilities.RolledRow<string>(Dice.Roll(100)).ToList();
                    var value = int.Parse(ability[2]);
                    if (value > 5) {
                        armament.BaseValue += value;
                    } else {
                        armament.Abilities += value;
                    }
                    armament.Qualities += ability[1] + " ";
                }
            }
            var b = armament.Bonus + armament.Abilities;
            armament.Value = armament.BaseValue + 300 + (2000 * (b * b));
            return armament;
        }

        private static Tuple<int, int> ParseBonus(string data) {
            if (data == "0") {
                return Tuple.Create(0, 0);
            }
            if (!data.Contains('x')) {
                return Tuple.Create(1, int.Parse(data));
            }
            return Tuple.Create(int.Parse(data.Substring(0, data.IndexOf('x'))), int.Parse(data.Substring(data.IndexOf('x') + 1)));
        }

        public static Armament Generate(string type) {
            var types = Data.Get(TypeTable);
            var weapon = types.RandomRowByValue<string, char>(3, type.ToCharArray()).ToList();
            return new Armament {
                Name = "Masterwork " + weapon[1],
                Value = 150 + int.Parse(weapon[2])
            };
        }
    }
}