﻿using pfuegen.Helpers;
using pfuegen.Treasure;
using System.Collections.Generic;
using System.Linq;

namespace pfuegen.Generators {
	public static class WandGenerator {
		public const string LevelTable = "7-42";
		public const string TypeTable = "7-43";
		public const string SpellTable = "7-44";
        public static Wand Generate(int grade) {
            var types = Data.Get(TypeTable);
            var type = types.RolledRow<int>(Dice.Roll(100)).ElementAt(1);
            var levels = Data.Get(LevelTable + "-" + grade);
            var level = levels.RolledRow<int>(Dice.Roll(100)).ElementAt(1);
            var spells = Data.Get(SpellTable + "-" + level + type);
            var data = spells.RolledRow<string>(Dice.Roll(100)).ToList();
            return CraftWand(type, level, data);
        }

		public static IEnumerable<Small> Generate(int quantity, int grade) {
			var small = new List<Small>();
			for (int i = 0; i < quantity; i++) {
				small.Add(Generate(grade));
			}
			return small;
		}

		private static Wand CraftWand(int type, int level, List<string> data) {
			return new Wand {
				Name = data[1],
				Value = int.Parse(data[2]),
				SpellLevel = level
			};
		}
	}
}