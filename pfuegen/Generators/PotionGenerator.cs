﻿using pfuegen.Helpers;
using pfuegen.Treasure;
using System.Collections.Generic;
using System.Linq;

namespace pfuegen.Generators {
	public static class PotionGenerator {
		public const string LevelTable = "7-14";
		public const string TypeTable = "7-15";
		public const string SpellTable = "7-16";
        public static Potion Generate(int grade) {
            var types = Data.Get(TypeTable);
            var type = types.RolledRow<int>(Dice.Roll(100)).ElementAt(1);
            var levels = Data.Get(LevelTable + "-" + grade);
            var level = levels.RolledRow<int>(Dice.Roll(100)).ElementAt(1);
            var spells = Data.Get(SpellTable + "-" + level + type);
            var data = spells.RolledRow<string>(Dice.Roll(100)).ToList();
            return BrewPotion(type, level, data);
        }

		public static IEnumerable<Potion> Generate(int quantity, int grade) {
			var potions = new List<Potion>();
			for (int i = 0; i < quantity; i++) {
                potions.Add(Generate(grade));	
			}
			return potions;
		}

		private static Potion BrewPotion(int type, int level, List<string> data) {
			return new Potion {
				Name = data[1],
				Value = int.Parse(data[2]),
				SpellLevel = level
			};
		}

	}
}