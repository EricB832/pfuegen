﻿using pfuegen.Helpers;
using pfuegen.Treasure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfuegen.Generators {
    public static class ArmorGenerator {
        public const string TypeTable = "7-12";
        public const string BonusTable = "3-2";
        public const string ArmorTable = "3-3";
        public const string ShieldTable = "3-4";
        public const string SpecificArmor = "3-5";
        public const string SpecificShield = "3-6";
        public static Armament Generate(int grade) {
            Armament armament = new Armament();
            var bonuses = Data.Get(BonusTable + "-" + grade);
            var bonus = bonuses.RolledRow<string>(Dice.Roll(100)).ToList();
            var abonus = ParseBonus(bonus[2]);
            if (abonus.Item1 == 0 && bonus[2] == "0") {
                var table = Dice.Roll(10) > 3 ? SpecificArmor : SpecificShield;
                var specifics = Data.Get(table + "-" + grade);
                var specific = specifics.RolledRow<string>(Dice.Roll(100)).ToList();
                armament.Name = specific[1];
                armament.Value = int.Parse(specific[2]);
                return armament;
            }
            
            var types = Data.Get(TypeTable);
            var type = types.RolledRow<string>(Dice.Roll(100)).ToList();
            armament.Name = type[1];
            armament.BaseValue = int.Parse(type[2]);
            armament.Bonus = int.Parse(bonus[1]);
            if (abonus.Item1 != 0) {
                var table = type[3] == "S" ? ShieldTable : ArmorTable;
                var abilities = Data.Get(table + "-" + abonus.Item2);
                for (int i = 0; i < abonus.Item1; i++) {
                    var ability = abilities.RolledRow<string>(Dice.Roll(100)).ToList();
                    var value = int.Parse(ability[2]);
                    if (value > 5) {
                        armament.BaseValue += value;
                    } else {
                        armament.Abilities += value;
                    }
                    armament.Qualities += ability[1] + " ";
                }
            }
            var b = armament.Bonus + armament.Abilities;
            armament.Value = armament.BaseValue + 150 + (1000 * (b * b));
            return armament;
        }

        public static Armament Generate(string type) {
            var types = Data.Get(TypeTable);
            var armor = types.RandomRowByValue<string, char>(3, type.ToCharArray()).ToList();
            return new Armament {
                Name = "Masterwork " + armor[1],
                Value = 150 + int.Parse(armor[2])
            };
        }

        private static Tuple<int, int> ParseBonus(string data) {
            if (data == "0"){
                return Tuple.Create(0, 0);
            }
            if (!data.Contains('x')){
                return Tuple.Create(1, int.Parse(data));
            }
            return Tuple.Create(int.Parse(data.Substring(0, data.IndexOf('x'))), int.Parse(data.Substring(data.IndexOf('x') + 1)));    
        }

        
    }
}