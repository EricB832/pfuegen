﻿using pfuegen.Helpers;
using pfuegen.Treasure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace pfuegen.Generators {
    public static class ArmsGenerator {
        public const string Table = "7-7";
        public static IEnumerable<Armament> Generate(int value) {
            Debug.WriteLine("Arms: " + value);
            var data = Data.Get(Table);
            var grades = data.RandomRowByValue<string>(value).ToList();
            var armaments = new List<Armament>();
            if (grades[1] != "0") {
                var types = grades[1].Split(',');
                foreach (var type in types) {
                    int grade;
                    if (int.TryParse(type, out grade)) {
                        armaments.Add(ArmorGenerator.Generate(grade));
                    } else {
                        armaments.Add(ArmorGenerator.Generate(type));
                    }
                }
            }
            if (grades[2] != "0") {
                foreach (var type in grades[2].Split(',')) {
                    int grade;
                    if (int.TryParse(type, out grade)) {
                        armaments.Add(WeaponGenerator.Generate(grade));
                    } else {
                        armaments.Add(WeaponGenerator.Generate(type));
                    }
                }
            }
            foreach (var a in armaments.Where(a => a.Bonus > 0)) {
                var special = Dice.Roll(100);
                a.Cursed = special == 1;
                a.Intelligent = special == 100;
            }
            return armaments;
        }
    }
}