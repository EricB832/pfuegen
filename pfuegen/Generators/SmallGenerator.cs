﻿using pfuegen.Helpers;
using pfuegen.Treasure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace pfuegen.Generators {
    public static class SmallGenerator {
        public const string Table = "7-6";

        public static Tuple<Coins, IEnumerable<Small>> Generate(int value) {
            Debug.WriteLine("Small: " + value);
            var data = Data.Get(Table);
            var values = data.RandomRowByValue<string>(value).ToList();
            return Tuple.Create(GetCoins(values), GetSmall(values));
        }

        private static IEnumerable<Small> GetSmall(List<string> values) {
            var small = new List<Small>();
            var scrolls = ParseSmall(values[4]);
            var potions = ParseSmall(values[5]);
            var wands = ParseSmall(values[6]);
            var objects = 
                ScrollGenerator.Generate(scrolls.Item1, scrolls.Item2).Concat(
                PotionGenerator.Generate(potions.Item1, potions.Item2)).Concat(
                WandGenerator.Generate(wands.Item1, wands.Item2));

            foreach (var s in objects) {
                s.Cursed = Dice.Roll(100) == 1;
            }
            return objects;
        }

        private static Tuple<int, int> ParseSmall(string data) {
            if (data == "0"){
                return Tuple.Create(0, 0);
            }
            if (!data.Contains('x')){
                return Tuple.Create(1, int.Parse(data));
            }
            return Tuple.Create(int.Parse(data.Substring(0, data.IndexOf('x'))), int.Parse(data.Substring(data.IndexOf('x') + 1)));    
        }

        private static Coins GetCoins(List<string> values) {
            return new Coins {
                Silver = Dice.Roll(values[1]),
                Gold = Dice.Roll(values[2]),
                Platinum = Dice.Roll(values[3])
            };
        }

    }
}