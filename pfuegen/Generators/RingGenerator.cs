﻿using pfuegen.Helpers;
using pfuegen.Treasure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfuegen.Generators {
    public static class RingGenerator {
        public const string Table = "4-1";
        public static Wondrous Generate(int grade) {
            var rings = Data.Get(Table);
            var ring = rings.RolledRowByGrade<string>(Dice.Roll(100), 3, grade).ToList();
            var cursed = Dice.Roll(100) == 1;
            var intelligent = Dice.Roll(100) == 100;
            return new Wondrous {
                Cursed = cursed,
                Intelligent = intelligent,
                Name = ring[1],
                Value = int.Parse(ring[2])
            };
        }
    }
}