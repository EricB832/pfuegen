﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using pfuegen.Helpers;
using System.Threading.Tasks;
using pfuegen.Generators;
using System.Diagnostics;

namespace pfuegen.Treasure {
    public static class CoinGemGenerator  {
        public const string Table = "7-4";

        public static Tuple<Coins, IEnumerable<Gem>> Generate(int value) {
            Debug.WriteLine("Gems: " + value);
            var data = Data.Get(Table);
            var values = data.RandomRowByValue<string>(value).ToList();
            return Tuple.Create(GetCoins(values), GetGems(values));
        }

        private static IEnumerable<Gem> GetGems(List<string> values) {
            return GenerateGems(Convert.ToInt32(values[5]), Convert.ToInt32(values[6]), Convert.ToInt32(values[7]),
                                Convert.ToInt32(values[8]), Convert.ToInt32(values[9]), Convert.ToInt32(values[10]));
        }

        private static Coins GetCoins(List<String> values) {
            return new Coins {
                Copper = Dice.Roll(values[1]),
                Silver = Dice.Roll(values[2]),
                Gold = Dice.Roll(values[3]),
                Platinum = Dice.Roll(values[4])
            };
        }

        private static IEnumerable<Gem> GenerateGems(int g1, int g2, int g3, int g4, int g5, int g6) {
            int[] grades = { g1, g2, g3, g4, g5, g6 };
            var gems = new List<Gem>();
            for (int i = 0; i < 6; i++) {
                for (int j = 0; j < grades[i]; j++) {
                    gems.Add(GenerateGem(i + 1));
                }
            }
            return gems;
        }

        public static Gem GenerateGem(int grade) {
            var roll = Dice.Roll(100);
            var worked = roll % 2 == 1;
            var data = Data.Get("7-50-" + grade);
            var values = data.RolledRow<string>(roll).ToList();
            var name = values[1];
            var value = Convert.ToInt32(values[2]) + Dice.Roll(values[3]);
            if (!worked) value /= 2;

            return new Gem {
                Name = name,
                Value = value,
                Worked = worked
            };
        }
    }
}