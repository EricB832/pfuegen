﻿using pfuegen.Helpers;
using pfuegen.Treasure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace pfuegen.Generators {
    public static class CombatGearGenerator {
        public const string Table = "7-8";
        public static Tuple<Coins, IEnumerable<Potion>, IEnumerable<Armament>, IEnumerable<Wondrous>> Generate(int value) {
            Debug.WriteLine("Gear: " + value);
            var gear = Data.Get(Table);
            var data = gear.RandomRowByValue<string>(value).ToList();
            var coins = GetCoins(data);
            var potions = GetPotions(data);
            var arms = GetArms(data);
            var wondrous = GetWondrous(data);
            return Tuple.Create(coins, potions, arms, wondrous);
        }

        private static Coins GetCoins(List<String> data) {
            return new Coins {
                Silver = Dice.Roll(data[1]),
                Gold = Dice.Roll(data[2])
            };
        }

        private static IEnumerable<Potion> GetPotions(List<string> data) {
            var grades = ParseGear(data[3]);
            var potions = new List<Potion>();
            foreach (var g in grades) {
                potions.AddRange(PotionGenerator.Generate(g.Item1, g.Item2));
            }
            return potions;
        }

        private static IEnumerable<Armament> GetArms(List<string> data) {
            var arms = new List<Armament>();
            if (data[4] != "0") {
                int grade;
                if(int.TryParse(data[4], out grade)){
                    arms.Add(ArmorGenerator.Generate(grade));
                } else {
                    arms.Add(ArmorGenerator.Generate(data[4]));
                }
            }
            if (data[5] != "0") {
                int grade;
                if (int.TryParse(data[5], out grade)) {
                    arms.Add(WeaponGenerator.Generate(grade));
                } else {
                    arms.Add(WeaponGenerator.Generate(data[5]));
                }
            }
            return arms;
        }

        private static IEnumerable<Wondrous> GetWondrous(List<string> data) {
            var wondrous = new List<Wondrous>();
           
            var w = ParseGear(data[6]);
            foreach (var g in w) {
                for (int i = 0; i < g.Item1; i++) {
                    wondrous.Add(WondrousGenerator.Generate(g.Item2));
                }
            }
            
            var r = ParseGear(data[7]);
            foreach (var g in r) {
                for (int i = 0; i < g.Item1; i++) {
                    wondrous.Add(RingGenerator.Generate(g.Item2));
                }
            }
            return wondrous;
        }

        private static IEnumerable<Tuple<int, int>> ParseGear(string data) {
            return Generator.Parse(data);
        }
    }
}