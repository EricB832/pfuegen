﻿using pfuegen.Helpers;
using pfuegen.Treasure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfuegen.Generators {
    public static class HoardGenerator {
        public const string Table = "7-11";
        public static Tuple<Coins, IEnumerable<Armament>, IEnumerable<Wondrous>, IEnumerable<Small>, IEnumerable<Gem>, IEnumerable<Art>> Generate(int value) {
            var data = Data.Get(Table);
            var values = data.RandomRowByValue<string>(value).ToList();
            var coins = GetCoins(values);
            var arms = GetArms(values);
            var wondrous = GetWondrous(values);
            var small = GetSmall(values);
            var gems = GetGems(values);
            var art = GetArt(values);
            return Tuple.Create(coins, arms, wondrous, small, gems, art);
        }

        private static IEnumerable<Art> GetArt(List<string> values) {
            var art = Generator.Parse(values[15]);
            return Generator.Generate(art, ArtGenerator.GenerateArt);
        }

        private static IEnumerable<Gem> GetGems(List<string> values) {
            var gems = Generator.Parse(values[14]);
            return Generator.Generate(gems, CoinGemGenerator.GenerateGem);
        }

        private static IEnumerable<Small> GetSmall(List<string> values) {
            var potions = Generator.Parse(values[11]);
            var scrolls = Generator.Parse(values[12]);
            var wands = Generator.Parse(values[13]);
            var small = new List<Small>();
            small.AddRange(Generator.Generate(potions, PotionGenerator.Generate));
            small.AddRange(Generator.Generate(scrolls, ScrollGenerator.Generate));
            small.AddRange(Generator.Generate(wands, WandGenerator.Generate));
            return small;
        }

        private static IEnumerable<Wondrous> GetWondrous(List<string> values) {
            var rings = Generator.Parse(values[7]);
            var rods = Generator.Parse(values[8]);
            var staves = Generator.Parse(values[9]);
            var wondrous = Generator.Parse(values[10]);
            return Generator.Generate(wondrous, WondrousGenerator.Generate).Concat(
                Generator.Generate(rings, RingGenerator.Generate)).Concat(
                Generator.Generate(rods, RodGenerator.Generate)).Concat(
                Generator.Generate(staves, StaffGenerator.Generate));
        }

        private static IEnumerable<Armament> GetArms(List<string> values) {
            var armor = Generator.Parse(values[5]);
            var weapons = Generator.Parse(values[6]);
            return Generator.Generate(armor, ArmorGenerator.Generate).Concat(
                Generator.Generate(weapons, WeaponGenerator.Generate));
        }

        private static Coins GetCoins(List<string> values) {
            return new Coins {
                Copper = Dice.Roll(values[1]),
                Silver = Dice.Roll(values[2]),
                Gold = Dice.Roll(values[3]),
                Platinum = Dice.Roll(values[4])
            };
        }
    }
}