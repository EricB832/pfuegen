﻿using pfuegen.Helpers;
using pfuegen.Treasure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace pfuegen.Generators {
    public static class CoinGenerator {
        public const string Table = "7-3";

        public static Coins Generate(int value) {
            Debug.WriteLine("Coins: " + value);
            // Gets the row by value
            var data = Data.Get(Table);
            var rolls = data.Row<string>(data.Column<int>(0).Select((v, i) => new { value = v, index = i }).First(v => v.value == value).index).ToList();
            return new Coins {
                Copper = Dice.Roll(rolls[1]),
                Silver = Dice.Roll(rolls[2]),
                Gold = Dice.Roll(rolls[3]),
                Platinum = Dice.Roll(rolls[4])
            };
        }
    }
}