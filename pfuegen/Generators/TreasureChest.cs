﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using pfuegen.Helpers;
using pfuegen.Models;
using System.Diagnostics;
using System.Threading.Tasks;
using pfuegen.Generators;

namespace pfuegen.Treasure {
    public class TreasureChest {
        public Coins Coins = new Coins();
        public List<Gem> Gems = new List<Gem>();
        public List<Art> Art = new List<Art>();
        public List<Small> Small = new List<Small>();
        public List<Armament> Arms = new List<Armament>();
        public List<Wondrous> Wondrous = new List<Wondrous>();

        public static TreasureChest Generate(int budget, IEnumerable<TreasureType> types) {
            var treasure = new TreasureChest();
            var spent = 0;
            while (spent < budget) {
                var treasures = types.Select(t => treasure.Prepare(t, budget - spent));
                var purchases = treasures.Where(p => p.Item1 > 0);

                var purchase = purchases.ElementAt(Dice.Random.Next(purchases.Count()));
                purchase.Item2(purchase.Item1);
                spent += purchase.Item1;
            }
	        treasure.Sort();
            return treasure;
        }

	    private void Sort() {
		    Gems = Gems.OrderByDescending(g => g.Value).ThenBy(g => g.Name).ToList();
		    Art = Art.OrderByDescending(a => a.Value).ThenBy(a => a.Name).ToList();
		    Small = Small.OrderBy(s => s.SmallType).ThenByDescending(s => s.Value).ThenBy(s => s.Name).ToList();
            Arms = Arms.OrderBy(a => a.Value).ThenBy(a => a.Name).ToList();
            Wondrous = Wondrous.OrderBy(w => w.Value).ThenBy(w => w.Name).ToList();
	    }

        private static int MaxLessThan(string table, int budget) {
            return Data.Get(table).Values().Where(v => v <= budget).Concat(new[] { 0 }).Max();
        }

        private static Tuple<int, Action<int>> Setup(string table, int budget, Action<int> action) {
            return new Tuple<int, Action<int>>(MaxLessThan(table, budget), action);
        }

        private Tuple<int, Action<int>> Prepare(TreasureType type, int budget) {
            switch (type) {
                default:
                    return Setup(CoinGenerator.Table, budget, 
                        v => Coins.Add(CoinGenerator.Generate(v)));
                case TreasureType.B:
                    return Setup(CoinGemGenerator.Table, budget, 
                        v => {
                            var coinsAndGems = CoinGemGenerator.Generate(v);
                            Coins.Add(coinsAndGems.Item1);
                            Gems.AddRange(coinsAndGems.Item2);
                        });
                case TreasureType.C:
                    return Setup(ArtGenerator.Table, budget, 
                        v => Art.AddRange(ArtGenerator.Generate(v)));
                case TreasureType.D:
                    return Setup(SmallGenerator.Table, budget,
                        v => {
                            var small = SmallGenerator.Generate(v);
                            Coins.Add(small.Item1);
                            Small.AddRange(small.Item2);
                        });
                case TreasureType.E:
                    return Setup(ArmsGenerator.Table, budget,
                        v => Arms.AddRange(ArmsGenerator.Generate(v)));
                case TreasureType.F:
                    return Setup(CombatGearGenerator.Table, budget,
                        v => {
                            var gear = CombatGearGenerator.Generate(v);
                            Coins.Add(gear.Item1);
                            Small.AddRange(gear.Item2);
                            Arms.AddRange(gear.Item3);
                            Wondrous.AddRange(gear.Item4);
                        });
                case TreasureType.G:
                    return Setup(CasterGearGenerator.Table, budget,
                        v => {
                            var gear = CasterGearGenerator.Generate(v);
                            Coins.Add(gear.Item1);
                            Small.AddRange(gear.Item2);
                            Arms.AddRange(gear.Item3);
                            Wondrous.AddRange(gear.Item4);
                        });
                case TreasureType.H:
                    return Setup(LairGenerator.Table, budget,
                        v => {
                            var lair = LairGenerator.Generate(v);
                            Coins.Add(lair.Item1);
                            Arms.AddRange(lair.Item2);
                            Wondrous.AddRange(lair.Item3);
                            Small.AddRange(lair.Item4);
                            Gems.AddRange(lair.Item5);
                        });
                case TreasureType.I:
                    return Setup(HoardGenerator.Table, budget,
                        v => {
                            var hoard = HoardGenerator.Generate(v);
                            Coins.Add(hoard.Item1);
                            Arms.AddRange(hoard.Item2);
                            Wondrous.AddRange(hoard.Item3);
                            Small.AddRange(hoard.Item4);
                            Gems.AddRange(hoard.Item5);
                            Art.AddRange(hoard.Item6);
                        });
            }
        }

        private int GetValue() {
            int v = 0;
            v += Coins.Value;
            v += Gems.Sum(g => g.Value);
            v += Art.Sum(g => g.Value);
            v += Small.Sum(g => g.Value);
            v += Arms.Sum(g => g.Value);
            v += Wondrous.Sum(g => g.Value);
            return v;
        }

        private int GetPrice() {
            int v = 0;
            v += Coins.Price;
            v += Gems.Sum(g => g.Price);
            v += Art.Sum(g => g.Price);
            v += Small.Sum(g => g.Price);
            v += Arms.Sum(g => g.Price);
            v += Wondrous.Sum(g => g.Price);
            return v;
        }

        public string Value { get { return GetValue() + "g"; } }

        public string Price { get { return GetPrice() + "g"; } }

    }


}