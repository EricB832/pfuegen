﻿using pfuegen.Helpers;
using pfuegen.Treasure;
using System.Collections.Generic;
using System.Linq;

namespace pfuegen.Generators {
    public static class ScrollGenerator {
        public const string LevelTable = "7-20";
        public const string TypeTable = "7-21";
        public const string SpellTable = "7-22";

        public static Scroll Generate(int grade) {
            var types = Data.Get(TypeTable);
            var type = types.RolledRow<int>(Dice.Roll(100)).ElementAt(1);
            var levels = Data.Get(LevelTable + "-" + grade);
            var level = levels.RolledRow<int>(Dice.Roll(100)).ElementAt(1);
            var spells = Data.Get(SpellTable + "-" + level + type);
            var data = spells.RolledRow<string>(Dice.Roll(100)).ToList();
            return ScribeScroll(type, level, data);
        }
        public static IEnumerable<Small> Generate(int quantity, int grade) {
            var small = new List<Small>();
            for (int i = 0; i < quantity; i++) {
                small.Add(Generate(grade));
            }
            return small;
        }

        private static Scroll ScribeScroll(int type, int level, List<string> data) {
            return new Scroll {
                Name = data[1],
                Value = int.Parse(data[2]),
                SpellLevel = level,
                Type = GetScrollType(type)
            };
        }

        private static ScrollType GetScrollType(int type) {
            return type <= 2 ? ScrollType.Arcane : ScrollType.Divine;
        }
    }
}