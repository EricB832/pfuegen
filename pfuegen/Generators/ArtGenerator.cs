﻿using pfuegen.Helpers;
using pfuegen.Treasure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace pfuegen.Generators {
    public static class ArtGenerator {
        public const string Table = "7-5";
        public static IEnumerable<Art> Generate(int value) {
            Debug.WriteLine("Art: " + value);
            var data = Data.Get(Table);
            var grades = data.RandomRowByValue<int>(value).Skip(1);
            return GenerateArt(grades);
        }

        public static IEnumerable<Art> GenerateArt(IEnumerable<int> grades) {
            var grade = 0;
            var art = new List<Art>();
            foreach (var quantity in grades) {
                grade++;
                if (quantity == 0) {
                    continue;
                }
                for (int i = 0; i < quantity; i++) {
                    art.Add(GenerateArt(grade));
                }
            }
            return art;
        }

        public static Art GenerateArt(int grade) {
            var data = Data.Get("7-51-" + grade);
            var roll = Dice.Roll(100);
            var values = data.RolledRow<string>(roll).ToList();
            return new Art {
                Name = values[1],
                Value = int.Parse(values[2])
            };
        }
    }
}