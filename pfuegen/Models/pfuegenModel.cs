﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace pfuegen.Models {
// ReSharper disable InconsistentNaming
    public class pfuegenModel {
// ReSharper restore InconsistentNaming

        public IEnumerable<SelectListItem> Levels { get; set; }
        public IEnumerable<SelectListItem> Speeds { get; set; }
        public IEnumerable<SelectListItem> Amounts { get; set; }
        public IEnumerable<SelectListItem> CreatureTypes { get; set; }

        public pfuegenInputModel Input { get; set; }
    }

// ReSharper disable InconsistentNaming
    public class pfuegenInputModel {
// ReSharper restore InconsistentNaming

        public bool Reroll { get; set; }
        public bool MoreTreasureTypes { get; set; }
        public string CreatureInfo { get; set; }

        public int Level { get; set; }
        public Speed Speed { get; set; }
        public Amount Amount { get; set; }

        [DisplayName("Type")]
        public CreatureType CreatureType { get; set; }
    }

    public enum Speed {
        Medium,
        Slow,
        Fast 
    }

    public enum Amount {
        Standard,
        Incidental,
        Double,
        Triple,
    }

    public enum CreatureType {
        Aberration,
        Animal,
        Construct,
        Dragon,
        Fey,
        Humanoid,
        MagicalBeast,
        MonstrousHumanoid,
        Ooze,
        Outsider,
        Plant,
        Undead,
        Vermin,
    }

    public enum TreasureType {
        A,
        B,
        C,
        D,
        E,
        F,
        G,
        H,
        I
    }
}