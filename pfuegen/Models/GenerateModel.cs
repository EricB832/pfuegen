﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using pfuegen.Treasure;

namespace pfuegen.Models {
    public class GenerateModel {
        public TreasureChest TreasureChest { get; set; }
        public pfuegenInputModel Input { get; set; }
        public string TreasureJson { get; set; }
        
        [StringLength(100)]
        [DisplayName("Save this treasure: ")]
        public string TreasureName { get; set; }
    }
}