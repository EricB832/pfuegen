﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using pfuegen.Treasure;

namespace pfuegen.Models {
    public class TreasureModel {
        public TreasureChest TreasureChest { get; set; }
        public string Name { get; set; }
    }
}