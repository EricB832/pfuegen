﻿using System;
using Newtonsoft.Json;
using pfuegen.Helpers;
using pfuegen.Models;
using pfuegen.Treasure;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace pfuegen.Controllers
{
// ReSharper disable InconsistentNaming
    public class pfuegenController : Controller
// ReSharper restore InconsistentNaming
    {
        
        public ActionResult Index() {
            var model = Load();
            return View(model);
        }

        [HttpPost]
        public ActionResult Generate(pfuegenInputModel input) {
            var old = TempData["input"] as pfuegenInputModel;
            
            if (!input.Reroll) {
                if (old != null) {
                    old.MoreTreasureTypes = input.MoreTreasureTypes;
                    input = old;
                } else {
                    switch (input.CreatureType) {
                        case CreatureType.Aberration:
                            return Specify(input, "cunning");
                        case CreatureType.Construct:
                            return Specify(input, "guarding a treasure");
                        case CreatureType.Humanoid:
                            return Specify(input, "an entire community");
                        case CreatureType.Undead:
                            return Specify(input, "intelligent");
                    }
                }
            }

            var budget = GetBudget(input.Level, input.Speed, input.Amount);

            input.Reroll = true;
            var model = new GenerateModel {
                TreasureChest = TreasureChest.Generate(budget, GetTreasureTypes(input.CreatureType, input.MoreTreasureTypes)),
                Input = input
            };
            
            model.TreasureJson = JsonConvert.SerializeObject(model.TreasureChest, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Auto });
            return View(model);
        }

        [HttpPost]
        public ActionResult Share(string treasureName, string treasureJson) {
            var id = Data.Save(treasureName, treasureJson);
            return RedirectToAction("Treasure", new {id});
        }

        public ActionResult Treasure(Guid id) {
            var treasure = Data.Load(id);
            var model = new TreasureModel {
                Name = treasure.Item1,
                TreasureChest = JsonConvert.DeserializeObject<TreasureChest>(treasure.Item2, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Auto })
            };
            return View(model);
        }

        private int GetBudget(int level, Speed speed, Amount amount) {

            var wealth = Data.Get("7-1");
            var s = 0;
            switch (speed) {
                case Speed.Slow: s = 1; break;
                case Speed.Fast: s = 3; break;
                default: s = 2; break;
            }
            var raw = wealth.Cell<int>(s, level - 1);
            switch (amount) {
                default: return raw;
                case Amount.Double: return raw * 2;
                case Amount.Triple: return raw * 3;
                case Amount.Incidental: return raw / 2;
            }
        }

        private ActionResult Specify(pfuegenInputModel input, string info) {
            input.CreatureInfo = info;
            TempData["input"] = input;
            return View("Specify", input);
        }

        private IEnumerable<TreasureType> GetTreasureTypes(CreatureType creatureType, bool more) {
            switch (creatureType) {
                case CreatureType.Aberration:
                    return more
                               ? new[] {TreasureType.A, TreasureType.B, TreasureType.D, TreasureType.E, TreasureType.F, TreasureType.G, TreasureType.H}
                               : new[] {TreasureType.A, TreasureType.B, TreasureType.D, TreasureType.E};
                case CreatureType.Animal:
                    return new[] {TreasureType.A, TreasureType.B, TreasureType.D, TreasureType.E};
                case CreatureType.Construct:
                    return more 
                        ? new[] {TreasureType.B, TreasureType.C, TreasureType.E, TreasureType.F, TreasureType.H }
                        : new[] {TreasureType.E, TreasureType.F};
                case CreatureType.Dragon:
                    return new[] { TreasureType.A, TreasureType.B, TreasureType.C, TreasureType.H, TreasureType.I };
                case CreatureType.Fey:
                    return new[] { TreasureType.B, TreasureType.C, TreasureType.D, TreasureType.G };
                case CreatureType.Humanoid:
                    return more
                        ? new[] { TreasureType.A, TreasureType.B, TreasureType.D, TreasureType.E, TreasureType.F, TreasureType.G, TreasureType.H }
                        : new[] { TreasureType.A, TreasureType.B, TreasureType.D, TreasureType.E, TreasureType.F, TreasureType.G };
                case CreatureType.MagicalBeast:
                    return new[] { TreasureType.A, TreasureType.B, TreasureType.D, TreasureType.E };
                case CreatureType.MonstrousHumanoid:
                    return new[] { TreasureType.A, TreasureType.B, TreasureType.C, TreasureType.D, TreasureType.E, TreasureType.H };
                case CreatureType.Ooze:
                    return new[] { TreasureType.A, TreasureType.B, TreasureType.D };
                case CreatureType.Outsider:
                    return new[] { TreasureType.A, TreasureType.B, TreasureType.C, TreasureType.D, TreasureType.E, TreasureType.F, TreasureType.G, TreasureType.H, TreasureType.I };
                case CreatureType.Plant:
                    return new[] { TreasureType.A, TreasureType.B, TreasureType.D, TreasureType.E };
                case CreatureType.Undead:
                    return more 
                        ? new[] { TreasureType.A, TreasureType.B, TreasureType.D, TreasureType.E ,TreasureType.F, TreasureType.G }
                        : new[] { TreasureType.A, TreasureType.B, TreasureType.D, TreasureType.E };
                case CreatureType.Vermin:
                    return new[] { TreasureType.A, TreasureType.B, TreasureType.D };
            }
            return new[] {TreasureType.D};
        }

        private pfuegenModel Load() {
            return new pfuegenModel {
                Levels =
                    from level in Enumerable.Range(1, 20)
                    select new SelectListItem() {
                        Text = level.ToString(),
                        Value = level.ToString()
                    },
                Speeds = SelectListHelper.BuildSelectList<Speed>(),
                Amounts = SelectListHelper.BuildSelectList<Amount>(),
                CreatureTypes = SelectListHelper.BuildSelectList<CreatureType>(),
                Input = new pfuegenInputModel {
                    CreatureType = CreatureType.Dragon
                }
            };
        }

    }
}
