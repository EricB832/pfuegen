﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfuegen.Helpers {
    public class Dice {

        public static readonly Random Random = new Random();

        public static int Roll(string rollString) {
            if (String.IsNullOrWhiteSpace(rollString) || rollString == "0") return 0;
            var d = rollString.IndexOf('d');
            var m = Math.Max(rollString.IndexOf('x'), rollString.IndexOf('*'));
            var a = rollString.IndexOf('+');

            var v1 = (d > 0) ? Convert.ToInt32(rollString.Substring(0, d)) : 1;

            if (m > 0) {
                var v2 = Convert.ToInt32(rollString.Substring(d + 1, m - d - 1));
                var v3 = Convert.ToInt32(rollString.Substring(m + 1));
                return Roll(v1, v2)*v3;
            } else if (a > 0) {
                var v2 = Convert.ToInt32(rollString.Substring(d + 1, a - 2));
                var v3 = Convert.ToInt32(rollString.Substring(a + 1));
                return Roll(v1, v2) + v3;
            } else {
                var v2 = Convert.ToInt32(rollString.Substring(d + 1));
                return Roll(v1, v2);
            }
        }

        public static int Roll(int quantity, int die) {
            var total = 0;
            for (var i = 0; i < quantity; i++) {
                total += Roll(die);
            }
            return total;
        }

        public static int Roll(int die) {
            return Random.Next(die) + 1;
        }
    }
}