﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace pfuegen.Helpers {
    public class Data {
        private const string ConnectionString = "Server=tcp:pfuegen.database.windows.net,1433;Data Source=pfuegen.database.windows.net;Initial Catalog=pfuedata;Persist Security Info=False;User ID=pfuegen;Password=4DI&g0vFgXv9S9&k;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

        private static readonly SqlConnection Database;

        private List<List<string>> _values; 

        static Data() {
            Database = new SqlConnection(ConnectionString);
            Database.Open();
        }

        public static Data Get(string worksheet)
        {
            var sql = $"select * from [{worksheet}]";
            var adapter = new SqlDataAdapter(sql, Database);
            var ds = new DataSet();
            adapter.Fill(ds);
            
            return new Data {
                _values = (
                    from DataRow row in ds.Tables[0].Rows
                    select row.ItemArray.Select(i => i.ToString()).ToList()).ToList()
            };
        }

        public static Guid Save(string name, string json) {
            var id = Guid.NewGuid();
            var command = Database.CreateCommand();
            command.CommandText = "insert into treasure (id, name, json) values (?id, ?name, ?value)";
            command.Parameters.AddWithValue("?id", id);
            command.Parameters.AddWithValue("?name", name);
            command.Parameters.AddWithValue("?value", json);
            command.ExecuteNonQuery();
            return id;
        }

        public static Tuple<string,string> Load(Guid id) {
            var command = Database.CreateCommand();
            command.CommandText = "select name, json from treasure where id = ?id";
            command.Parameters.AddWithValue("?id", id);
            var adapter = new SqlDataAdapter(command);
            var ds = new DataSet();
            adapter.Fill(ds);
            return Tuple.Create(ds.Tables[0].Rows[0].Field<string>("name"), ds.Tables[0].Rows[0].Field<string>("json"));
        }

        public IEnumerable<T> Column<T>(int column) {
            return _values.Select(v => (T)Convert.ChangeType(v[column], typeof(T)));
        }

        public IEnumerable<T> Row<T>(int row) {
            return _values[row].Select(v => (T)Convert.ChangeType(v, typeof(T)));
        }
 
        public T Cell<T>(int column, int row) {
            return (T)Convert.ChangeType(_values[row][column], typeof(T));
        }

        public IEnumerable<T> RolledRow<T>(int roll) {
            var mins = Column<int>(0).ToList();
            for (var i = 1; i < mins.Count; i++) {
                if (mins[i] > roll) {
                    return Row<T>(i - 1);
                } 
            }
            return Row<T>(mins.Count - 1);
        }

        public IEnumerable<T> RolledRowByGrade<T>(int roll, int column, int grade) {
            var rows = (
                from row in _values
                where int.Parse(row[column]) == grade
                select row).ToList();

            for (var i = 1; i < rows.Count; i++) {
                if (int.Parse(rows[i][0]) > roll) {
                    return rows[i - 1].Select(v => (T)Convert.ChangeType(v, typeof(T)));
                }
            }
            return rows.Last().Select(v => (T)Convert.ChangeType(v, typeof(T)));
        }

        public IEnumerable<int> Values() {
            return Column<int>(0);
        }

        public IEnumerable<T> RandomRowByValue<T>(int value) {
            var rows = Column<int>(0).Select((x, i) => new { x, i }).Where(t => t.x == value).ToList();
            var random = rows.ElementAt(Dice.Random.Next(rows.Count)).i;
            return Row<T>(random);
        }

        public IEnumerable<T> RandomRowByValue<T, T2>(int column, params T2[] valid) {
            var rows = Column<T2>(column).Select((x, i) => new { x, i }).Where(t => valid.Contains(t.x)).ToList();
            var random = rows.ElementAt(Dice.Random.Next(rows.Count)).i;
            return Row<T>(random);
        }

    }

}