﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace pfuegen.Helpers {
    public static class SelectListHelper {
        public static IEnumerable<SelectListItem> BuildSelectList<T>() {
            return
                from T t in Enum.GetValues(typeof(T))
                select new SelectListItem {
                    Text = t.ToString(),
                    Value = t.ToString()
                };
        }
    }
}