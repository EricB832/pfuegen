﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfuegen.Helpers {
    public static class Generator {
        public static IEnumerable<Tuple<int, int>> Parse(string data) {
            if (data == "0") {
                return new Tuple<int, int>[0];
            }
            var tuples = new List<Tuple<int, int>>();
            foreach (var d in data.Split(',')) {
                if (d == "0") {
                    tuples.Add(Tuple.Create(0, 0));
                    continue;
                }
                if (!d.Contains('x')) {
                    tuples.Add(Tuple.Create(1, int.Parse(d)));
                    continue;
                }
                tuples.Add(Tuple.Create(int.Parse(d.Substring(0, d.IndexOf('x'))), int.Parse(d.Substring(d.IndexOf('x') + 1))));
            }
            return tuples;
        }

        public static IEnumerable<T> Generate<T>(IEnumerable<Tuple<int, int>> data, Func<int, T> generate) {
            var treasure = new List<T>();
            foreach (var d in data) {
                for (int i = 0; i < d.Item1; i++) {
                    treasure.Add(generate(d.Item2));
                }
            }
            return treasure;
        }
    }
}