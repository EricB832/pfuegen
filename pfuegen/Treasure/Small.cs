﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfuegen.Treasure {
	public abstract class Small : Treasure{
		public string Name { get; set; }

		public override string Text {
			get { return Format(Name, Value); }
		}

		public override int Value { get; set; }
		public override int Price { get { return Value / 2; } }
		public abstract SmallType SmallType { get; }
        public bool Cursed { get; set; }
    }

	public enum SmallType {
		Wand,
		Potion,
		Scroll
	}
}