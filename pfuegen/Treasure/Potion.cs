﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfuegen.Treasure {
	public class Potion : Small {
		public int SpellLevel { get; set; }

		public override string Text {
			get { return Format("(PO:" + SpellLevel + ") " + Name, Value); }
		}

		public override SmallType SmallType {
			get { return SmallType.Potion; }
		}
	}
}