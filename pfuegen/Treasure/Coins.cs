﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Web;
using pfuegen.Helpers;
using System.Threading.Tasks;

namespace pfuegen.Treasure {
    public class Coins : Treasure {
        
        public int Copper { get; set; }
        public int Silver { get; set; }
        public int Gold { get; set; }
        public int Platinum { get; set; }

        public void Add(Coins coins) {
            Copper += coins.Copper;
            Silver += coins.Silver;
            Gold += coins.Gold;
            Platinum += coins.Platinum;
        }

        public override string Text {
            get { return Format(Platinum, "p ") + Format(Gold, "g ") + Format(Silver, "s ") + Format(Copper, "c"); }
        }

        public override int Value {
            get { return (Copper / 100) + (Silver / 10) + Gold + (Platinum * 10); }
            set { return; }
        }
    }
}