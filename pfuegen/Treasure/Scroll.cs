﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfuegen.Treasure {
	public class Scroll : Small{
		public ScrollType Type { get; set; }
		public int SpellLevel { get; set; }

		public override string Text {
            get { return Format("(" + Format(Cursed, "Cursed ") + (Type == ScrollType.Arcane ? "A" : "D") + "S:" + SpellLevel + ") " + Name, Value); } 
		}

		public override SmallType SmallType {
			get { return SmallType.Scroll; }
		}
	}

	public enum ScrollType {
		Arcane,
		Divine
	}
}