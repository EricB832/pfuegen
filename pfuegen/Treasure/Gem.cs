﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using pfuegen.Helpers;
using System.Threading.Tasks;

namespace pfuegen.Treasure {
    public class Gem : Treasure{
        public string Name { get; set; }
        public bool Worked { get; set; }

        public override string Text {
            get { return Format(Format(!Worked, "Rough ") + Name, Value); }
        }
        
        public override int Value { get; set; }
    }
}
