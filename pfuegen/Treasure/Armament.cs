﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfuegen.Treasure {
    public class Armament : Treasure {
        public int BaseValue { get; set; }
        public int Bonus { get; set; }
        public int Abilities { get; set; }
        public string Qualities { get; set; }
        public string Name { get; set; }
        public bool Cursed { get; set; }
        public bool Intelligent { get; set; }

        public override string Text {
            get { return Format(Format(Cursed, "Cursed ") + Format(Intelligent, "Intelligent ") + Format(Bonus > 0, "+" + Bonus + " ") + ((Qualities ?? "") + Name), Value); }
        }

        public override int Value { get; set; }

        public override int Price { get { return Value / 2; } }
    }
}