﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfuegen.Treasure {
    public class Wondrous : Treasure{
        public bool Cursed { get; set; }
        public bool Intelligent { get; set; }
        public string Name { get; set; }

        public override string Text {
            get { return Format(Format(Intelligent, "Intelligent ") + Format(Cursed, "Cursed ") + Name, Value); }
        }

        public override int Value { get; set; }

        public override int Price { get { return Value / 2; } }
    }
}