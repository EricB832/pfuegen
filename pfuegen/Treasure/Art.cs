﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfuegen.Treasure {
    public class Art : Treasure {
        public string Name { get; set; }

        public override string Text {
            get { return Format(Name, Value); }
        }

        public override int Value { get; set; }
    }
}