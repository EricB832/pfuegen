﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfuegen.Treasure {
    public abstract class Treasure {

        public abstract string Text { get; }
        public abstract int Value { get; set; }
        public virtual int Price { get { return Value; } }

        protected string Format(bool condition, string text) {
            return condition ? text : "";
        }

        protected string Format(int value, string text) {
            return value > 0 ? value + text : "";
        }

        protected string Format(string name, int value) {
            return name + ": " + value + "g";
        }
    }
}